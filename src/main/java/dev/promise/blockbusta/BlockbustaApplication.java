package dev.promise.blockbusta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class BlockbustaApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlockbustaApplication.class, args);
	}
}
