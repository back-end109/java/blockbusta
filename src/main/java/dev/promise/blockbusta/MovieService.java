package dev.promise.blockbusta;


import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;
import java.util.Optional;

@Service
public class MovieService {
//     database access methods
    @Autowired
    private MovieRepository movieRepository; // let framework know to instantiate this class automatically
    public List<Movie> allMovies () {

       return movieRepository.findAll();
    }



    public Optional<Movie> singleMovie(String imdbId) {
        return movieRepository.findMovieByImdbId(imdbId);
    }
}
